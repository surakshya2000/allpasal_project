var buttonsTab, contentTab;

		buttonsTab = document.querySelectorAll(".tabContainer .buttonContainer button");
		contentTab = document.querySelectorAll(".tabContainer .tabContent");

		function showContent(contentIndex,colorCode) {
			buttonsTab.forEach(function(menu){
				menu.style.backgroundColor = "";
				menu.style.color = "";	
			});
			buttonsTab[contentIndex].style.backgroundColor = colorCode;
			buttonsTab[contentIndex].style.color = "black";


			contentTab.forEach(function(menu){
				menu.style.display = "none";
			});
			contentTab[contentIndex].style.display = "block";
			contentTab[contentIndex].style.backgroundColor = colorCode;
		}
		showContent(0, '#eae2b7');