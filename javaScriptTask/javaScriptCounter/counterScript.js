var counterDisplay ,buttonInc ,buttonDec , buttonClr;
var count = 0;

counterDisplay =document.getElementById("screen");
buttonInc = document.getElementById("increment");
buttonDec = document.getElementById("decrement");
buttonClr = document.getElementById("clear");

buttonInc.onclick = function() {
	count += 1;
	counterDisplay.innerHTML = count ;
}

buttonDec.onclick = function() {
	count -= 1;
	counterDisplay.innerHTML = count ;
}

buttonClr.onclick = function() {
	count = 0;
	counterDisplay.innerHTML = count ;
}