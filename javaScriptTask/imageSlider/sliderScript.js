	var i = 0;
	var images = [];

	images[0] = 'portfolioImage1.jpg';
	images[1] = 'portfolioImage2.jpg';
	images[2] = 'interior1.jpg';
	images[3] = 'interior2.jpg';
	
	function changeImg(){
		document.imageSlide.src = images[i];

		if(i < images.length - 1){
			i++;
		} else {
			i = 0;
		}

		setTimeout("changeImg()", 2000);
	}

	window.onload = changeImg;