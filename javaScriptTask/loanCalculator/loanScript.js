function calculateLoan() {

	var amount,rate,months,interest,result

	 amount = document.querySelector("#principalAmount").value;

	 rate = document.querySelector("#interestRate").value;

	 months = document.querySelector("#time").value;

	// interest per month
	 interest = (amount * (rate * 0.01)) / months;
	
	// Calculating total payment
	 result = ((amount / months) + interest).toFixed(2);

	document.querySelector("#total").innerHTML = "Payment Amount:  ₹  " + result;
	document.getElementById("myForm").reset();
}