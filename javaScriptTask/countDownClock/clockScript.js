var deadlineTime = new Date("jan 01, 2022 12:12:12").getTime();
  
var x = setInterval(function() {
  
        var currentTime = new Date().getTime();

        var timeLeft = deadlineTime - currentTime;

        var days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
        var hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24))/(1000 * 60 * 60));
        var minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);

        document.getElementById("day").innerHTML = days ;
        document.getElementById("hour").innerHTML = hours;
        document.getElementById("minute").innerHTML = minutes; 
        document.getElementById("second").innerHTML = seconds;

        if (timeLeft < 0) {
                clearInterval(x);
                
                document.getElementById("notify").innerHTML = "TIME EXPIRED !";
                document.getElementById("day").innerHTML ='0';
                document.getElementById("hour").innerHTML ='0';
                document.getElementById("minute").innerHTML ='0' ; 
                document.getElementById("second").innerHTML = '0'; }
}, 1000);